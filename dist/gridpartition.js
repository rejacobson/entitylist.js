(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.GridPartition = factory();
    }
}(this, function () {

  if (!Array.isArray) {
    Array.isArray = function(arg) {
      return Object.prototype.toString.call(arg) === '[object Array]';
    };
  };

  //                  [w, h]   [w, h]
  var Grid = function(mapsize, cellsize) {
    if (!Array.isArray(mapsize)  || mapsize.length != 2)   throw new Error('EntityList map_size must be a 2 element array: [map_w, map_h]');
    if (!Array.isArray(cellsize) || cellsize.length != 2)  throw new Error('EntityList cells must be a 2 element array: [cell_w, cell_h]');

    // The size of the area being partitioned.
    this._mapsize = mapsize;

    // The number of cells in each of the x and y axis
    this._cellcount = [Math.round(mapsize[0] / cellsize[0]),
                       Math.round(mapsize[1] / cellsize[1])];

    // Recalculate the cell sizes based off of the x and y cell counts
    this._cellsize = [mapsize[0] / this._cellcount[0], mapsize[1] / this._cellcount[1]];

    // The largest cell index in the map
    this._maxindex = this._cellcount[0] * this._cellcount[1] - 1;

    // Keep track of the number of elements in each cell
    // _residents[cell_index] == number of elements
    this._residents = {};

    this._adjacent_indices_cache  = [];

    // Array of grid cells
    // This holds the entities/values being partitioned
    this._map = [];

    this.length = 0;
  };

  Grid.prototype = {
    indexOutOfBounds: function(index) {
      return index < 0 || index > this._maxindex;
    },

    positionOutOfBounds: function(position) {
      return position[0] < 0 ||
             position[0] >= this._mapsize[0] ||
             position[1] < 0 ||
             position[1] >= this._mapsize[1];
    },

    // Get a cell x and y coordinates by a cell index
    indexToPosition: function(index) {
      if ('number' != typeof index) throw('#indexToPosition - index is not a number: '+ index);
      return [(index % this._cellcount[0]) * this._cellsize[0],
              ~~(index / this._cellcount[0]) * this._cellsize[1]];
    },

    indexToCoords: function(index) {
      if ('number' != typeof index) throw('#indexToPosition - index is not a number: '+ index);
      return [index % this._cellcount[0], ~~(index / this._cellcount[0])];
    },

    positionToIndex: function(position) {
      if (!position.length) throw('#positionToIndex - position is not an array: '+ position);
      if (this.positionOutOfBounds(position)) return null;
      var x = position[0] / this._cellsize[0];
      var y = position[1] / this._cellsize[1];
      x = x|x;
      y = y|y;
      return (y * this._cellcount[0]) + x;
    },

    // Return the stored element counts.
    // Only cells with more than one element in them are represented.
    getResidents: function() {
      return this._residents;
    },

    // Add a new element to the grid, in the specified cell index
    insert: function(index, value) {
      var cell = this.getCell(index);
      if (!cell) return;
      cell.push(value);
      this.length++;
      if (!this._residents[index]) this._residents[index] = 0;
      this._residents[index]++; // increment the number of elements in this cell
    },

    // Remove an element from the grid and return it
    remove: function(index, value) {
      var cell = this.getCell(index);
      if (!cell) return;

      for (var i=0; i<cell.length; ++i) {
        if (cell[i] == value) {
          cell.splice(i, 1);
          this.length--;
          if (--this._residents[index] <= 0) delete this._residents[index];
          break;
        }
      }

      return value;
    },

    getCell: function(index) {
      if (this.indexOutOfBounds(index)) return;
      return this._map[index] || (this._map[index] = []);
    },

    getCells: function(indices) {
      var cells = [];
      var len   = indices.length;
      var c, j, l;
      for (var i=0; i<len; i++) {
        c = this.getCell(indices[i]);
        l = c.length;
        for (j=0; j<l; j++) {
          cells.push(c[j]);
        }
      };
      return cells;
    },

    // Get the contents of all cells surrounding the cell at index
    getAdjacentCells: function(index) {
      return this.getCells(this.getAdjacentIndices(index));
    },

    // Get a list of all indices surrounding the cell at index
    getAdjacentIndices: function(index) {
      if (this._adjacent_indices_cache[index]) return this._adjacent_indices_cache[index];
      var indices = [];
      var dirs = [[-1, -1], [0, -1], [1, -1], [-1, 0], [1, 0], [-1, 1], [0, 1], [1, 1]];
      var self = this;
      dirs.map(function(i) { indices = indices.concat(self.getLinearIndices(index, i, 1)); });
      return (this._adjacent_indices_cache[index] = indices);
    },

    // Get a list of all indices in a straight line from index, excluding the current index
    getLinearIndices: function(index, direction, limit) {
      direction   = [direction[0] / Math.abs(direction[0]), direction[1] / Math.abs(direction[1])];
      limit       = limit || Infinity;

      if (!direction[0] && !direction[1]) throw('#getLinearIndices - A valid direction is required '+ direction);

      var indices = [];
      var coords  = this.indexToCoords(index);
      var limit_x = direction[0] ? Math.abs((direction[0] > 0 ? this._cellcount[0]-1 : 0) - coords[0]) : Infinity;
      var limit_y = direction[1] ? Math.abs((direction[1] > 0 ? this._cellcount[1]-1 : 0) - coords[1]) : Infinity;

      limit = Math.min(limit, limit_x, limit_y);

      for (var i=0; i<=limit; i++) {
        if (i) indices.push(index);
        if (direction[0]) index += direction[0];
        if (direction[1]) index += direction[1] * this._cellcount[0];
      }

      return indices;
    }
  };

  return Grid;

}));
