(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['lodash'], factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory(require('lodash'));
    } else {
        // Browser globals (root is window)
        root.EntityList = factory(root._);
    }
}(this, function (_) {

/*

  Entities stored in the EntityList require a position property, that contains a 2d array:

    entity.position == [x, y]

  These Entity methods are optional and will be called if they exist:
    update(dt)
    remove()

  var entity = {
    _position: [x, y],

    position: function() {
      return this._position;
    },

    update: function(dt) {

    },

    remove: function() {
      // called when an entity is removed from the entity list
    }
  };

*/

  var EntityList = function(spatial_partition) {
    this.partition  = spatial_partition;
    this.entities   = [];
  };

  EntityList.prototype = {
    length: function() {
      return this.partition.length;
    },

    getEntities: function() {
      return _.compact(this.entities);
    },

    insert: function(insertMe) {
      if (_.isArray(insertMe)) {
        this._insertEntities(insertMe);
      } else {
        this._insertEntity(insertMe);
      }
    },

    _insertEntity: function(entity) {
      if (entity._cell_index >= 0) return;

      var cell_index = this.partition.positionToIndex(entity.position());

      this.entities.push(entity);

      this.reindex(cell_index, entity);
    },

    _insertEntities: function(entities) {
      for (var i=0, len = entities.length; i<len; i++) {
        this._insertEntity(entities[i]);
      }
    },

    remove: function(removeMe) {
      if (_.isArray(removeMe)) {
        this._removeEntities(removeMe);
      } else {
        this._removeEntity(removeMe);
      }
    },

    _removeEntity: function(entity) {
      if (entity._cell_index) this.partition.remove(entity._cell_index, entity);

      var i = _.indexOf(this.entities, entity);
      if (-1 != i) {
        this.entities[i] = undefined;
      }

      entity._cell_index = undefined;

      if (entity.remove) entity.remove();
    },

    _removeEntities: function(entities) {
      for (var i=0, len = entities.length; i<len; i++) {
        this._removeEntity(entities[i]);
      }
    },

    // Reposition the entity within the spatial partition
    reindex: function(index, entity) {
      if (index == entity._cell_index) return;
      if (entity._cell_index) this.partition.remove(entity._cell_index, entity);
      entity._cell_index = index;
      this.partition.insert(index, entity);
    },

    // Execute a function for each entity in the system
    eachEntity: function(func) {
      for (var i=0, len = this.entities.length; i<len; ++i) {
        if (!this.entities[i]) continue;
        if (false === func.call(this, this.entities[i], i)) break;
      }
    },

    // Update all entities
    update: function(dt) {
      var e, new_index, dead = [];

      this.eachEntity(function(e) {
        if (e.update) e.update(dt);

        new_index = this.partition.positionToIndex(e.position());
        if (null === new_index) {
          dead.push(e);
        } else if (e._cell_index != new_index) {
          this.reindex(new_index, e);
        }
      });

      this.remove(dead);
    }
  };

  return EntityList;
}));
